import * as express from 'express';
import "reflect-metadata";
import {createConnection} from "typeorm";
import * as appConfig from "./common/app-config";
import { InversifyExpressServer } from 'inversify-express-utils';
import container from './inversify.config';

/**
 * Create Express server.
 */
const app = express();

let server =  new InversifyExpressServer(container, null, { rootPath: "/api" }, app);

let appConfigured = server.build();
let serve = appConfigured.listen(process.env.PORT || 3000, () => `App running on ${serve.address().port}`);

/**
 * Create connection to DB using configuration provided in 
 * appconfig file.
 */
createConnection(appConfig.dbOptions).then(async connection => {
    console.log("Connected to DB");

}).catch(error => console.log("TypeORM connection error: ", error));

module.exports = app;
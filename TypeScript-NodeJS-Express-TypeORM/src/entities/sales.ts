import {Entity, Column,  PrimaryGeneratedColumn} from "typeorm";

@Entity("sales")
export class Sales {

    @PrimaryGeneratedColumn()
    productId: number;

    @Column({
        length: 100
    })
    name: string;

    
    count: number;    

    stockPending:number
}
import {Entity, Column,  PrimaryGeneratedColumn} from "typeorm";

@Entity("product")
export class Product {

    @PrimaryGeneratedColumn()
    productId: number;

    @Column({
        length: 100
    })
    name: string;    
}
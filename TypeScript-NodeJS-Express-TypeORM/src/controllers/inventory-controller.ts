import * as express from 'express';
import { interfaces, controller, httpGet, request, response, httpPost, httpPut } from "inversify-express-utils";
import { InventoryRepository } from '../repositories/inventory-repository';
import { inject } from 'inversify';
import TYPES from '../type';

@controller("/")
export class InventoryController implements interfaces.Controller {

    private inventoryRepository: InventoryRepository;
constructor(@inject(TYPES.InventoryRepository) repository: InventoryRepository) {
    this.inventoryRepository = repository;
}

  @httpGet("/item")
  public async getProduct (@request() req: express.Request, @response() res: express.Response) {
    try {
      const posts = this.inventoryRepository.getProduct(req.query);
      res.status(200).json(posts);
    } catch(error) {
      res.status(400).json(error);
    }
  }

  @httpPost("/add")
  public async addProduct (@request() req: express.Request, @response() res: express.Response) {
    try {
      const posts = this.inventoryRepository.addToProduct(req.body);
      res.status(200).json(posts);
    } catch(error) {
      res.status(400).json(error);
    }
  }

  @httpPut("/edit")
  public async editProduct (@request() req: express.Request, @response() res: express.Response) {
    try {
      const posts = this.inventoryRepository.UpdateSales(req.body);
      res.status(200).json(posts);
    } catch(error) {
      res.status(400).json(error);
    }
  }
}
import { injectable } from "inversify";
import { Product } from "../entities/product";
import { getManager } from "typeorm";
import { Sales } from "../entities/sales";

@injectable()
export class InventoryRepository{

    getProduct(product: Product){
        // get product
        return getManager().getRepository(Product).find(product); 
    }

    addToProduct(product: Product){
        // add product, if new "add" else "update"
        return getManager().getRepository(Product).save(product); 
    }

    deleteFromInventory(productId: number){
        // delete product from inventory based on id
        return getManager().getRepository(Product).deleteById(productId); 
    }

    UpdateSales(productIds:number){
        let item:Sales;
        // update item count in sales
        getManager().getRepository(Sales).findOneById(productIds).then(value=> {value.count=value.count-1, item=value}); 
        
        // update sales by product id
        return getManager().getRepository(Sales).save(item); 
    }
}
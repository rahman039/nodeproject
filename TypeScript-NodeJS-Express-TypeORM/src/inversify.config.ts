import TYPES from './type';
import {Container} from 'inversify';
import { InventoryRepository } from './repositories/inventory-repository';

const container = new Container();

container.bind<InventoryRepository>(TYPES.InventoryRepository ).to(InventoryRepository).inSingletonScope();
export default container;
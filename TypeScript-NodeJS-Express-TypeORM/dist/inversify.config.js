"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const type_1 = require("./type");
const inversify_1 = require("inversify");
const inventory_repository_1 = require("./repositories/inventory-repository");
const container = new inversify_1.Container();
container.bind(type_1.default.InventoryRepository).to(inventory_repository_1.InventoryRepository).inSingletonScope();
exports.default = container;
//# sourceMappingURL=inversify.config.js.map
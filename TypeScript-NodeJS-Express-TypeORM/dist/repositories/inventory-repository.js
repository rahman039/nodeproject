"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const product_1 = require("../entities/product");
const typeorm_1 = require("typeorm");
const sales_1 = require("../entities/sales");
let InventoryRepository = class InventoryRepository {
    getProduct(product) {
        // get product
        return typeorm_1.getManager().getRepository(product_1.Product).find(product);
    }
    addToProduct(product) {
        // add product, if new "add" else "update"
        return typeorm_1.getManager().getRepository(product_1.Product).save(product);
    }
    deleteFromInventory(productId) {
        // delete product from inventory based on id
        return typeorm_1.getManager().getRepository(product_1.Product).deleteById(productId);
    }
    UpdateSales(productIds) {
        let item;
        // update item count in sales
        typeorm_1.getManager().getRepository(sales_1.Sales).findOneById(productIds).then(value => { value.count = value.count - 1, item = value; });
        // update sales by product id
        return typeorm_1.getManager().getRepository(sales_1.Sales).save(item);
    }
};
InventoryRepository = __decorate([
    inversify_1.injectable()
], InventoryRepository);
exports.InventoryRepository = InventoryRepository;
//# sourceMappingURL=inventory-repository.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const windowsDriver = require("mssql/msnodesqlv8");
exports.dbOptions = {
    type: "mssql",
    host: "RCHENADATH01",
    port: 1434,
    database: "inventory",
    extra: {
        driver: windowsDriver,
        trustedConnection: true
    },
    options: {
        useUTC: true
    },
    entities: [
        "./entities/*.js"
    ],
    synchronize: true,
};
// export let dbOptions: ConnectionOptions = {
//      type: "mssql",
//      host: "",
//      port: 1433,
//      username: "",
//      password: "",
//      database: "inventory",
//      entities: [
//           "./entities/*.js"
//      ],
//      synchronize: true,
//  }
// createConnection({
//      type: "mssql",
//      host: "localhost",
//      port: 1433,
//      database: "NodeTypeOrm",
//      extra: {
//          driver: windowsDriver,
//          trustedConnection: true
//      },
//      options: {
//          useUTC: true,
//          trustedConnection: true
//      }
//  }).then(connection => {
//      console.log('Connected');
//  }).catch(error => {
//      console.log(error);
//  });
//# sourceMappingURL=app-config.js.map